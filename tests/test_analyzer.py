import bganalyzer.analyzer as bgan

def test_extract():
    y = [1, 2, 3, 4, 5, 4, 3, 2, 1, 6, 7, 8, 2, 3]
    x = list(range(len(y)))

    result = bgan.extract(x, y, lambda x: x < 4)
    assert result == [([0, 1, 2], [1, 2, 3]), 
                      ([6, 7, 8], [3, 2, 1]), 
                      ([12, 13], [2, 3])]

    result = bgan.extract(x, y, lambda x: x > 4)
    assert result == [([4], [5]), 
                      ([9, 10, 11], [6, 7, 8])]
